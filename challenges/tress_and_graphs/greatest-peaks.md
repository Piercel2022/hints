# Trees and Graphs: The Greatest Peaks

<!--  Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

A peak is an element which holds greater value than its neighbors. The below illustration shows how to define an element as a peak.

<img src="images/trees_and_graphs/greatest-peaks/greatest-peaks-1.png"  width="800"><br>

<b>`Hint:`&nbsp;Starting from the current element, traverse through its maximum neighbor until it meets with a peak. <br>

<b>`Note:`</b>&nbsp;You need to use row and column indexes when you process each element of the grid. Using two nested "for" loops is the best way to traverse through a grid.<br>
<b>`Note:`</b>&nbsp;You need to find each element's neighbors. The recommended way is to use a helper method that returns the neighbors of a given element as an array.<br>
<b>`Note:`</b>&nbsp;Keep in mind that diagonal elements don't count as neighbors. Only top, bottom, left and right elements can be defined as neighbors.
  
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;How to find the peak starting from a given element</summary><br>

In this challenge, you can use the depth-first search (DFS) algorithm for finding the peak of an element.

A typical DFS algorithm uses an ordering method that picks any unvisited nodes from the neighbors of the current node.

However, in this problem, you should use a different ordering method. You know which path leads to the solution - always move to the maximum neighbor instead of moving to an unvisited node.

The below illustration presents how to find the peak starting from a given element.

<img src="images/trees_and_graphs/greatest-peaks/greatest-peaks-2.png"  width="800"><br>

<b>`Hint:`&nbsp;Get the current element's neighbors. Next, find the element that holds the greatest value among the current element and its neighbors.<br>
<b>`Hint:`&nbsp;If the maximum value is the current element, then the current element is a peak.<br>
<b>`Hint:`&nbsp;If the maximum value is not the current element, then visit the element which holds maximum value in the next step of DFS.<br>

</details>
<!-- Hint 2 -->

---

<!-- Hint 3 -->
<details>
  <summary><b>Hint 3:[CODE]</b>&nbsp;Structure of the algorithm</summary><br>

```ruby
def greatest_peaks(map)
  # store peaks in a hash
  # key: current grid position [i, j] value: peak position [i, j]
  # hash[[0, 0]] = [2, 0]
  # above information says that the element placed at [0, 0], 
  # is controlled by the peak placed at [2, 0]
  peaks = { }
  # this loop helps you to process each element of the given grid
  for i in 0...map.length
    for j in map...length
      # implement the depth-first search algorithm here
      # protip: it might be extracted to some helper method
    end
  end
  # loop produces a hash that contains visited elements with their peaks
  # the next step is processing this data
  # find peaks that occur the most often and the least often
  # then return them as an array [min_occured_peak, max_occured_peak]
end

# helper method
def neighbors_of(map, i, j)
  # each element can own max four neighbors	
  # a neighbor placed on its top    -> above row -> i - 1
  # a neighbor placed on its bottom -> below row -> i + 1
  # a neighbor placed on its left   -> left column -> j + 1
  # a neighbor placed on its right  -> right column -> j - 1
end
```

<b>`Hint:`&nbsp;DFS algorithm visits each unvisited element which has a connection with the current element. In this challenge, you don't need this logic. You need to use DFS to traverse through maximum neighbors.<br>
<b>`Hint:`&nbsp;When you find the peak at the end of the search algorithm, store it in the peaks hash.<br>

<b>`Note:`</b>&nbsp;Don't forget to process the current element while looking for the maximum value. The current element may hold the maximum value.<br>
<b>`Note:`</b>&nbsp;Keep in mind that **neighbors_of** method needs to respect the grid's boundaries. An element placed in the first row (i = 0)  hasn't got a top neighbor.<br>
  
</details>
<!-- Hint 3 -->

---

<!-- Solution 1 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Naive Solution (Recursive) - not optimized</summary><br>

```ruby
def greatest_peaks(map)
  peaks = { }

  for i in 0...map.length
    for j in 0...map[i].length
      current = [i, j]
      # find the peak of the current position
      peaks[[i, j]] = search_peak(map, current)
    end
  end
  # calculate the occurrences of each peak  
  peaks_counter = peaks.values.uniq.map { |peak| [peak, peaks.values.count(peak)] }.to_h
  # return the minimum and maximum occurred peaks
  [peaks_counter.values.min, peaks_counter.values.max]
end

def search_peak(map, current)
  # get neighbors of the current element
  neighbors = neighbors_of(map, current[0], current[1])
  # find the element that holds maximum value
  maximum = (neighbors.push(current)).max_by do |element| 
    map[element.first][element.last]
  end

  # if the current element holds the maximum, then it is the peak
  return current if maximum == current
  # repeat for the maximum
  search_peak(map, maximum)
end

# helper function
def neighbors_of(map, row, col)
  # returns all neighbors of an element
end
```
  
</details>
<!-- Solution 1 -->

---

<!-- Solution 2 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Optimized Solution (Recursive)</summary><br>

This problem can be optimized by the memoization technique. You can watch the video in this [link](https://www.youtube.com/watch?v=OQ5jsbhAv_M&feature=youtu.be&t=656) (10:56 - 15:20)
  
```ruby
def greatest_peaks(map)
  peaks = { }

  for i in 0...map.length
    for j in 0...map[i].length
      current = [i, j]
      peaks[[i, j]] = search_peak(map, current, peaks)
    end
  end
  peaks_count = peaks.values.uniq.map { |e| [e, peaks.values.count(e)] }.to_h
  [peaks_count.values.min, peaks_count.values.max]
end

def search_peak(map, current, peaks)
  neighbors = neighbors_of(map, current[0], current[1])

  maximum = (neighbors.push(current)).max_by do |element| 
    map[element.first][element.last]
  end
  # memoization
  # if peaks[maximum] is present, then it is already calculated
  # no need to calculate it again
  return peaks[maximum] if peaks[maximum]
  return current if maximum == current
  
  search_peak(map, maximum, peaks)
end

# helper function
def neighbors_of(map, row, col)
  # returns all neighbors of an element
end
```

</details>
<!-- Solution 2 -->

---

<!-- Solution 3 -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Optimized Solution (Iterative) - only for curious 😎</summary><br>

```ruby
def greatest_peaks(map)
  peaks = { }

  for i in 0...map.length
    for j in 0...map[i].length
      stack = [[i, j]]

      until stack.empty?
        current = stack.last
        neighbors = neighbors_of(map, current[0], current[1])
        maximum = (neighbors.push(current)).max_by do |element| 
          map[element.first][element.last]
        end

        # memoization
        if peaks[maximum]
          stack.each { |e| peaks[e] = peaks[maximum] }
          break
        end

        if maximum == current
          stack.each { |e| peaks[e] = maximum }
          break
        end
        stack.push(maximum)
      end
    end
  end
  peaks_count = peaks.values.uniq.map { |e| [e, peaks.values.count(e)] }.to_h
  [peaks_count.values.min, peaks_count.values.max]
end

# helper function
def neighbors_of(map, row, col)
  # returns all neighbors of an element
end
```

<b>`Note:`</b>&nbsp;Recursive way to build the DFS algorithm is much easier than the iterative one. As opposed to this, iterative way to build the BFS algorithm is much easier than the recursive one. <br>
  
</details>
<!-- Solution 3 -->

