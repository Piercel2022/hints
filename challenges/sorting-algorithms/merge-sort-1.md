<!-- Hint 1 -->
<details>
  <summary><b>Hint 1:</b>&nbsp;Basis of the problem</summary><br>

Merge sort is a Divide-and-Conquer algorithm. The steps are:<br>
&emsp;\- **Divide:** Divides the input array into two halves.<br>
&emsp;\- **Conquer:** Repeatedly calls itself in each half until there is only one element remaining (an array with only one element is always sorted).<br>
&emsp;\- **Combine:** Merges the two sorted halves resulting from the Conquer step.<br>

---

<br>

In this challenge, we will focus on the **combine** step. Merging is a key process in **Merge Sort** which assumes that the two given arrays are sorted and combines the sorted arrays into one. 

There are a couple of different merging algorithms. Here, we will use **one-pass-merge**, so called because it only needs to make one pass through the data.

Three function properties are required:<br>
&emsp;\- an array to store the merged elements; name it **result**<br>
&emsp;\- an index value for traversing through the first array; name it "**i**"<br>
&emsp;\- an index value for traversing through the second array; name it "**j**"<br>

<img src="images/sorting-algorithms/merge-sort-1/merge-sort-1-1.png"  width="800"><br>

<b>`Hint:`&nbsp;Traverse both arrays until one of them is completely traversed.<br>
<b>`Hint:`&nbsp;At each traverse step, find the lesser of the current elements from the two arrays, update the result array with the smaller value, and increment its index to the next position.<br>
<b>`Hint:`&nbsp;If elements still remain in the incomplete array, place all remaning elements into the result array.<br>
</details>
<!-- Hint 1 -->

---

<!-- Hint 2 -->
<details>
  <summary><b>Hint 2:</b>&nbsp;A step-by-step analysis</summary><br>

The algorithm completes its process using three while loops. The first loop traverses both arrays at the same time until one is completely traversed. At that time, there will be elements remaining in the other array. If it is the first array that has elements left, the second loop stores these remaining elements into the merged array. Otherwise, the third loop does this for the second array.

The pictures below show how to merge the arrays **[1, 3, 9, 11]** and **[2, 4, 6, 8]**:

<img src="images/sorting-algorithms/merge-sort-1/merge-sort-1-2.png"  width="1000"><br>
<img src="images/sorting-algorithms/merge-sort-1/merge-sort-1-3.png"  width="1000">

As you can see from the iteration above, the second array is completely traversed,and its index "**j**" exceeds the length of the array. There are elements left in the first array, so we will use another loop to store all remaining elements into the merged array.

<img src="images/sorting-algorithms/merge-sort-1/merge-sort-1-4.png"  width="800">

<b>`Hint:`&nbsp;The first loop traverses both arrays at the same time. During the traversal, compare the current elements of the two arrays. Then, copy the smaller element into the result array and move the index of the array containing the smaller element.<br>
<b>`Hint:`&nbsp;Continue through the first loop until the index value exceeds the length of the array.<br>
<b>`Hint:`&nbsp;If elements still remain in the first array after the first loop, use the second loop to append them to the result array.<br>
<b>`Hint:`&nbsp;Or, if elements still remain in the second array after the first loop, use the third loop to append them to the result array.<br>
  
</details>
<!-- Hint 2 -->

---

<!-- Solution -->
<details>
  <summary><b>[SPOILER ALERT!]</b>&nbsp;Solution</summary><br>

```ruby
def merge_sort(array1, array2)
  # third array to store the merged elements
  result = []
  # index for the first array
  i = 0
  # index for the second array
  j = 0
  # iterate until one of them is completely traversed
  while i < array1.length && j < array2.length
    # if the first array holds the smaller element in its current index,
    # store it in the result array then increment the index of the first array
    if array1[i] <= array2[j]
      result << array1[i]
      i += 1
    # otherwise, store the element from the second array in the result array,
    # then increment the index of the second array
    else
      result << array2[j]
      j += 1
    end
  end
  # after the loop above, one of the arrays is completely traversed but the other is not
  # this means that elements remain
  
  # append all remaining elements from the first array to the result array, if they exist
  while i < array1.length
    result << array1[i]
    i += 1
  end

  # append all remaining elements from the second array to the result array, if they exist
  while j < array2.length
    result << array2[j]
    j += 1
  end
  result
end
```

</details>
<!-- Solution -->
